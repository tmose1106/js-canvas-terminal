# rollup-typescript-template

Simple template project using [Yarn](https://yarnpkg.com/),
[rollup](https://rollupjs.org/guide/en/) and
[TypeScript](https://www.typescriptlang.org/) designed for use with VS Code.

Just clone using [degit](https://github.com/Rich-Harris/degit) or something
of the like.

Then using Yarn you can add packages and everything should play nicely.

```bash
degit https://gitlab.com/tmose1106/rollup-typescript-template.git my-new-project
yarn add three                # Add a libray as a dependency
yarn install                  # Get all project and dev depencies
yarn rollup --config --watch  # Build your project to src/app.js on changes
```

Also includes configuration for
[VS Code Live Server](https://github.com/ritwickdey/vscode-live-server) plugin
to make your life a little easier.

## Plug n Play

By default, Yarn Berry is included. Rather than using node_modules, it uses a
custom "plug n play" format which generates a `.pnp.js` file to tell Node
where to import libraries from. Read more about it
[here](https://yarnpkg.com/features/pnp).

I have decided to not use this, and just run `yarn install` every time. If you
want to use PnP, just comment the associated lines in `.gitignore`.
