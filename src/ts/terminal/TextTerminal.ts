export default class TextTerminal {
    context: CanvasRenderingContext2D;
    options: TextTerminalOptions;
    fontMap: FontMapSettings;
    font: FontSettings;
    cursor: Rect;

    constructor(canvas: HTMLCanvasElement, fontMap: HTMLImageElement, options: TextTerminalOptions) {
        this.context = canvas.getContext('2d');

        this.options = options;

        this.fontMap = {
            map: fontMap,
            width: Math.floor(fontMap.width / 8),
            height: Math.floor(fontMap.height / 16)
        };

        // console.log(this.fontMap);

        this.font = {
            width: Math.floor(canvas.width / options.columns),
            height: Math.floor(canvas.height / options.rows)
        };

        this.cursor = {
            x: 0,
            y: 0,
            w: 1,
            h: 1
        };
    }

    setCursor(x: number, y: number) {
        this.cursor.x = x;
        this.cursor.y = y;
    }

    moveCursor(dx: number, dy: number) {
        this.cursor.x += dx;
        this.cursor.y += dy;
    }

    _getCharPositionInMap(keyCode: number): Rect {
        // return {
        //     x: (keyCode % 8) * this.fontMap.width,
        //     y: Math.floor(keyCode / 8) * this.fontMap.height,
        //     w: this.fontMap.width,
        //     h: this.fontMap.height
        // };
        return {
            x: (keyCode % 8) * this.fontMap.width,
            y: Math.floor(keyCode / 8) * this.fontMap.height + 8,
            w: this.fontMap.width - 8,
            h: this.fontMap.height - 16
        };
    }

    _getCharPositionOnCanvas(x: number, y: number): Rect {
        return {
            x: x * this.font.width,
            y: y * this.font.height,
            w: this.font.width,
            h: this.font.height
        }
    }

    clearChar(): void;
    clearChar(x: number, y: number): void;

    clearChar(x?: number, y?: number) {
        if (!x || !y) {
            x = this.cursor.x, y = this.cursor.y;
        }

        const canvasPosition = this._getCharPositionOnCanvas(x, y);

        this.context.clearRect(canvasPosition.x, canvasPosition.y, canvasPosition.w, canvasPosition.h);
    }

    printChar(keyCode: number, x?: number, y?: number) {
        const fontMapPosition = this._getCharPositionInMap(keyCode);
        const canvasPosition = x === null || y === null ? this._getCharPositionOnCanvas(x, y) : this._getCharPositionOnCanvas(this.cursor.x, this.cursor.y);

        // console.log(canvasPosition, fontMapPosition);

        this.context.drawImage(
            this.fontMap.map,
            fontMapPosition.x, fontMapPosition.y, fontMapPosition.w, fontMapPosition.h,
            canvasPosition.x, canvasPosition.y, canvasPosition.w, canvasPosition.h
        );

        // this.context.strokeRect(canvasPosition.x, canvasPosition.y, canvasPosition.w, canvasPosition.h);
    }

    printString(string: string): void;
    printString(string: string, x?: number, y?: number): void;

    printString(string: string, x?: number, y?: number) {
        if (!x || !y) {
            x = this.cursor.x, y = this.cursor.y;
        }
        for (let i = 0; i < string.length; ++i) {
            this.printChar(string.charCodeAt(i), x + i, y);
            this.moveCursor(1, 0);
        }
    }

    printTest() {
        for (let i = 0; i < 128; ++i) {
            const column = i % 16 + 1, row = Math.floor(i / 16) + 1;
            this.printChar(i, column, row);
        }
    }

    _onKeyDown(event) {
        console.log(event);
    }

    attach(element: HTMLElement) {
        element.addEventListener('keydown', (event) => this._onKeyDown(event));
    }
}

interface Rect {
    x: number;
    y: number;
    w: number;
    h: number;
}

// Details on how to get characters from the font map
interface FontMapSettings {
    map: HTMLImageElement;
    width: number;
    height: number;
}

// Details on how to render characters on the canvas
interface FontSettings {
    width: number;
    height: number;
}

interface TextTerminalOptions {
    columns: number;
    rows: number;
}