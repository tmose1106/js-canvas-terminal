import Shell from './Shell';
import TextTerminal from './terminal/TextTerminal';

const canvas = document.getElementById('terminal-canvas') as HTMLCanvasElement;

async function getImage(url: string) {
    const response = await fetch(url);

    const imageData = await response.blob();

    const imageElement = new Image();
    imageElement.src = URL.createObjectURL(imageData);

    return imageElement;
}

getImage('img/font_map.png').then((fontMap) => {
    fontMap.addEventListener('load', () => {
        console.log(fontMap);

        const terminal = new TextTerminal(canvas, fontMap, {
            columns: 40,
            rows: 20
        });

        // terminal.printTest();

        // terminal.printString('Ted', 20, 10);

        // terminal.clearChar(21, 10);

        const shell = new Shell(terminal, '> ');

        document.addEventListener('keydown', (event) => shell.onControl(event));
        document.addEventListener('keypress', (event) => shell.onCharacter(event));
    })
});