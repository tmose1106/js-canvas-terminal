import TextTerminal from './terminal/TextTerminal';

export default class Shell {
    term: TextTerminal;
    prompt: string;
    buffer: string;

    constructor(term: TextTerminal, prompt: string) {
        this.term = term;
        this.prompt = prompt;

        this.buffer = '';

        this.term.setCursor(0, 0);
        this.term.printString('Welcome to TERM', 0, 0);
        this.term.moveCursor(-15, 1);
        this.printPrompt();
    }

    printPrompt() {
        this.term.printString(this.prompt);
    }

    onCharacter(event: KeyboardEvent) {
        if (event.key === 'Enter') return;
        // console.log(this.term.cursor);
        this.term.printChar(event.key.charCodeAt(0));
        this.term.moveCursor(1, 0);

        this.buffer += event.key;
    }

    onControl(event: KeyboardEvent) {
        // console.log(event);

        switch (event.key) {
            case 'Backspace':
                if (this.term.cursor.x <= this.prompt.length) break;
                this.term.moveCursor(-1, 0);
                this.term.clearChar();
                this.buffer = this.buffer.slice(0, this.buffer.length - 1);
                break;
            case 'Enter':
                this.term.setCursor(0, this.term.cursor.y + 1);
                this.term.printString(this.buffer);
                this.buffer = '';
                this.term.setCursor(0, this.term.cursor.y + 1);
                this.printPrompt();
                break;
            default:
                break;
        }
    }
}